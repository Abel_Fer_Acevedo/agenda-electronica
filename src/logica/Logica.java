package logica;

import igu.Interfaz;

/**
 *
 * @author Abel
 */
public class Logica {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Interfaz agenda = new Interfaz();
        agenda.setVisible(true);            // Lo hago visible
        agenda.setLocationRelativeTo(null); // centro

    }

}
